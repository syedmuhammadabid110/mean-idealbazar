import { Router } from '@angular/router';
import { FormBuilder, Validators, FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastComponent } from '../shared/toast/toast.component';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../shared/models/user.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  user: User;
  isLoading = true;

  registerForm: FormGroup;
  fname = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(30),
    Validators.pattern('[a-zA-Z0-9_-\\s]*')
  ]);
  lname = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(30),
    Validators.pattern('[a-zA-Z0-9_-\\s]*')
  ]);
  gender = new FormControl('', [
    Validators.required
  ]);
  username = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(30),
    Validators.pattern('[a-zA-Z0-9_-\\s]*')
  ]);
  email = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(100),
    this.isEmail
  ]);
  password = new FormControl('', [
    Validators.required,
    Validators.minLength(6)
  ]);
  confirmPassword = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    this.isEqualPassword.bind(this)
  ]);
  // role = new FormControl('', [
  //   Validators.required
  // ]);
  constructor(private auth: AuthService,
    public toast: ToastComponent,
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fname: this.fname,
      lname: this.lname,
      gender: this.gender,
      username: this.username,
      email: this.email,
      password: this.password,
      confirmPassword: this.confirmPassword,
      role: 'user'
    });

    this.getUser();
  }

  getUser() {
    this.userService.getUser(this.auth.currentUser).subscribe(
      data => this.user = data,
      error => console.log(error),
      () => this.isLoading = false
    );
  }

  save(user: User) {
    this.userService.editUser(user).subscribe(
      res => {
        this.toast.setMessage('Account settings saved!', 'success');
        this.router.navigate(['/']);
      },
      error => console.log(error)
    );
  }

  setClassFirstname() {
    return { 'has-danger': !this.fname.pristine && !this.fname.valid };
  }
  setClassLastname() {
    return { 'has-danger': !this.lname.pristine && !this.lname.valid };
  }
  setClassUsername() {
    return { 'has-danger': !this.username.pristine && !this.username.valid };
  }

  setClassEmail() {
    return { 'has-danger': !this.email.pristine && !this.email.valid };
  }

  setClassPassword() {
    return { 'has-danger': !this.password.pristine && !this.password.valid };
  }

  setClassConfirmPassword() {
    return { 'has-danger': !this.confirmPassword.pristine && !this.confirmPassword.valid };
  }
  isEmail(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)) {
      return { noEmail: true };
    }
  }

  isEqualPassword(control: FormControl): { [s: string]: boolean } {
    if (!this.registerForm) {
      return { passwordsNotMatch: true };

    }
    if (control.value !== this.registerForm.controls['password'].value) {
      return { passwordsNotMatch: true };
    }
  }
}

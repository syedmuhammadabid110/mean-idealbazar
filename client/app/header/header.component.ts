import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  cartProducts: any;
  constructor(public auth: AuthService) { }

  ngOnInit() {
    this.initiateData();
  }

  initiateData() {
    let data = localStorage.getItem('cart');
    let bill = 0;
    if (data !== null) {
      this.cartProducts = JSON.parse(data);
      console.log(this.cartProducts.length);
      this.bill = 0;
      for (let i in this.cartProducts) {
        this.cartProducts[i]["qt"] = 1;
        this.bill = this.bill + this.cartProducts[i].price * this.cartProducts[i].qt;
      }
    } else {
      this.cartProducts = [];
    }
  }
}
